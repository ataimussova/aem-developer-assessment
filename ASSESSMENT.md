Overview
========

This test is designed to let you demonstrate your skills in developing AEM
components. It is also intended to give you an opportunity to show off your
knowledge of design patterns, best practice and anything else you think might
impress us.

You may use 3rd party libraries as you see fit, however if you use any code that
is not your own work you **must** credit the source.

If you are not able to complete the entire task in the time provided, focus on
implementing as much of the specification as you can into a functional
application rather than trying to implement more into an application that
doesn't work.

A base project template is provided (see the submission guidelines) which
contains the skeleton for the project, based on Adobe's AEM 6 project archetype.
It is not mandatory that you use any particular part of this code – you are free
to expand, rearrange, or discard it as you see fit.

Please read the task and assessment guidelines carefully before you begin.

 

The task
========

Your task is to implement a component for an AEM-based contact database website
for university staff members and departments. The component to be created is
a "Contact card" component, which allows authors to select from the contacts
available in the system and display their details. Authors should be able to
drop the component into a page and then select the contact they wish to
display. The result should resemble this wireframe:

![Component front-end wireframe](<resources/component.png>)

Dialog
------

The component should have a dialog with a single path field that allows the user
to browse to the desired contact in the JCR and select it:

![Component dialog wireframe](<resources/dialog.png>)

Error states
------------

If the author has not yet configured a contact, or if the node they selected is
not a valid contact the component should display a message to the author: "Edit
this component to select a contact" or "Invalid contact selected, edit this
component to select a valid contact". You may replace these messages with
something more appropriate to your solution if you wish, provided the messages
still give the author useful, actionable information.

Additional features
-------------------

You may implement additional features if you wish, provided they are consistent
with the overall purpose of the component.

Sample content
--------------

The base project provided includes a content package (ui.content),
which contains a site skeleton and some pages containing contact details for
some people and departments. The site skeleton has three pages:

| Page         | Location                       | Purpose                                                                                                                                                                  |
|--------------|--------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Home         | /content/contacts/home         | Landing page                                                                                                                                                             |
| Key contacts | /content/contacts/key-contacts | Lists some key contacts, using the contact card component you will implement. Contacts to be displayed are: "ICT Help Desk (id: org-y5)" and "Dr John Test (id: j-test)" |

Your components will need to display information for staff and departments.
Sample data for these components is provided in pages
under */content/contacts/profiles* and */content/contacts/departments* respectively.
In all, the following contact pages are provided as part of the content package:

-   /content/contacts/people/d/d-jessup

-   /content/contacts/people/g/g-poole

-   /content/contacts/people/j/j-clements

-   /content/contacts/people/j/j-test

-   /content/contacts/departments/o/org-163

-   /content/contacts/departments/o/org-168

-   /content/contacts/departments/o/org-y5

Within each contact page, the details for that contact are stored inside a
component called "profile". Properties available are:

| Property   | Type     | Description                                                                           |
|------------|----------|---------------------------------------------------------------------------------------|
| id         | String   | Unique identifier for this contact within the University's phonebook system           |
| building   | String   | Code of the University building where the contact is located                          |
| department | String   | The related department or faculty                                                     |
| name       | String   | The name of the person or department                                                  |
| title      | String   | Contact's title (if any) e.g. Professor *(“people" contacts only)*                    |
| position   | String   | Contact's job title *("people" contacts only)*                                        |
| type       | String[] | Contact type designation. Can be one or more of: “people”, “staff” and “organisation" |
| address    | String[] | Mailing address, line by line                                                         |
| email      | String[] | Contact email address(es)                                                             |
| url        | String[] | Contact's website(s)                                                                  |
| phone      | String[] | Contact phone number(s)                                                               |

 

How you will be assessed
========================

-   After completing the task we will discuss with you how your solution works
    and ask any questions we have about your approach. You will have a chance to
    highlight any features you think are significant or any problems you
    encountered.

-   Your application will be tested to see it functions as specified.

-   Your code will be reviewed for technique and style, including ease of reuse,
    use of language and framework features (to the extent required to complete
    the task), and overall readability.

-   If you had time to implement any additional features not within the
    specification these will be considered on a case-by-case basis.

 

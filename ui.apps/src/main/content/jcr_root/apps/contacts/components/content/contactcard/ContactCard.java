package apps.contacts.components.content.contactcard;

import com.adobe.cq.sightly.WCMUse;
import org.apache.sling.api.resource.Resource;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ContactCard extends WCMUse {
    private Node contactNode;

    @Override
    public void activate() throws Exception {
        String contactPath = getProperties().get("contactPath", "");
        Resource contactResource = getResourceResolver().getResource(contactPath + "/jcr:content/profile");
        if (contactResource != null) {
            contactNode = contactResource.adaptTo(Node.class);
        }
    }

    public String getName() throws RepositoryException {
        String contactName = "";
        if (contactNode != null) {
            if (contactNode.hasProperty("title")) {
                contactName = contactNode.getProperty("title").getString() + " ";
            }
            if (contactNode.hasProperty("name")) {
                contactName += contactNode.getProperty("name").getString() + " ";
            }
        }

        return contactName;
    }

    public String getPosition() throws RepositoryException {
        String position = "";
        if (contactNode != null) {
            if (contactNode.hasProperty("position")) {
                position = contactNode.getProperty("position").getString() + ", ";
            }
            if (contactNode.hasProperty("department")) {
                position += contactNode.getProperty("department").getString();
            }
        }

        return position;
    }

    public List<String> getPhoneList() throws RepositoryException {
        List<String> phones = new ArrayList();
        if (contactNode != null) {
            if (contactNode.hasProperty("phone")) {
                List<Value> phoneValues = Arrays.asList(contactNode.getProperty("phone").getValues());
                for (Value value : phoneValues) {
                    phones.add(value.getString());
                }
            }
        }

        return phones;
    }

    public List<String> getEmailList() throws RepositoryException {
        List<String> email = new ArrayList();
        if (contactNode != null) {
            if (contactNode.hasProperty("email")) {
                List<Value> emailValues = Arrays.asList(contactNode.getProperty("email").getValues());
                for (Value value : emailValues) {
                    email.add(value.getString());
                }
            }
        }

        return email;
    }
}
